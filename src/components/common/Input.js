/**
 * Created by Jakk on 9/26/2016 AD.
 */
import React from 'react';
import {TextInput, View, Text} from 'react-native';

const Input = ({label, onChangeText, value, placeholder, secure}) => {
    const {inputStyle, lableStyle, containerStyle} = styles;
    return (
        <View style={containerStyle}>
            <Text style={lableStyle}>{label}</Text>
            <TextInput
                autoCorrect={false}
                placeholder={placeholder}
                style={inputStyle}
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={secure}
            />
        </View>
    );
};

const styles = {
    inputStyle: {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 18,
        lineHeight: 23,
        flex: 2,
        width: 100,
        height: 40
    },
    lableStyle: {
        fontSize: 16,
        paddingLeft: 20,
        flex: 1
    },
    containerStyle: {
        height: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }
};

export {Input};