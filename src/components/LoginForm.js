/**
 * Created by Jakk on 9/25/2016 AD.
 */
import React, {Component} from 'react';
import firebase from 'firebase';
import {Text} from 'react-native';
import {Card, CardSection, Button, Input, Spinner} from './common'

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false
        };
        this.onButtonPressed = this.onButtonPressed.bind(this);
        this.renderButton = this.renderButton.bind(this);
        this.onLoginSuccess = this.onLoginSuccess.bind(this);
        this.onLoginFail= this.onLoginFail.bind(this);
    }

    onButtonPressed() {
        const {email, password, error, loading} = this.state;
        this.setState({
            error: '',
            loading: true
        });
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess())
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(this.onLoginSuccess())
                    .catch(this.onLoginFail());
            });

    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner size="small"/>;
        } else {
            return <Button
                whenPressed={this.onButtonPressed}
                buttonText="Sign in"/>;
        }
    }

    onLoginSuccess() {
        console.log('SUCCESSFULLY LOGIN');
        this.setState({
            email: '',
            password: '',
            loading: false,
            error: ''
        });
    }

    onLoginFail() {
        this.setState({
            error: 'Authentication failed',
            loading: false,
        });
    }

    render() {
        console.log(this.state.email);
        console.log(this.state.password);
        return (
            <Card>
                <CardSection>
                    <Input
                        value={this.state.email}
                        onChangeText={text => this.setState({email: text})}
                        placeholder="username@email.com"
                        label="Email"/>
                </CardSection>

                <CardSection>
                    <Input
                        value={this.state.password}
                        onChangeText={text => this.setState({password: text})}
                        placeholder="password"
                        secure={true}
                        label="Password"/>
                </CardSection>

                <Text style={styles.errorTextStyle}>
                    {this.state.error}
                </Text>

                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
};
export default LoginForm;