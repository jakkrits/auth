import React, { Component } from 'react';
import firebase from 'firebase';
import {Text, View} from 'react-native';
import {Header} from './components/common'
import LoginForm from './components/LoginForm';
import {Button, Spinner} from './components/common';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: null
        };
        this.renderContent = this.renderContent.bind(this);
        this.logUserOut = this.logUserOut.bind(this);
    }

    componentWillMount() {
        firebase.initializeApp({
            apiKey: "AIzaSyD0B4zVgAX_1fBbVvSEdBW0hFkhbkIu6Jo",
            authDomain: "auth-36f31.firebaseapp.com",
            databaseURL: "https://auth-36f31.firebaseio.com",
            storageBucket: "auth-36f31.appspot.com",
            messagingSenderId: "360254405190"
        });

        firebase.auth().onAuthStateChanged((user) => {
            if(user) {
                this.setState({loggedIn: true});
            } else {
                this.setState({loggedIn: false});
            }
        });
    }

    logUserOut() {
        firebase.auth().signOut();
    }

    renderContent() {
        switch(this.state.loggedIn) {
            case true:  return <Button whenPressed={this.logUserOut} buttonText="Log out"/>;
            case false: return <LoginForm/>;
            default: return <Spinner size="large"/>
        }
    }

    render() {
        return (
            <View>
                <Header headerText="Auth"/>
                {this.renderContent()}
            </View>
        );
    }
}

export default App;
